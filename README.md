# prepare_data_for_mysql

#### 介绍
快速生成测试数据

#### 软件架构
```
root /data/git/prepare_data_for_mysql > tree -L 2
.
├── conf                             #配置文件相关的目录
│   ├── mysql.xml                    #mysql连接配置信息，用于获取表结构等信息，必须正确配置
│   ├── prepare.xml                  #主程序配置文件，用户需要根据配置文件中提示配置
│   └── template.xml                 #主程序配置文件模板
├── data                             #程序造数据保存的目录
│   └── data_file_1                  #程序造数据保存的文件，data_file_n(n是分片号)
├── Help.txt                         #作者联系方式
├── log                              #日志目录
│   ├── error.log                    #错误日志文件
│   ├── length.log                   #程序生成的临时文件，请忽略
│   ├── tmp.log                      #程序生成的临时文件，请忽略
│   └── tty.log                      #程序生成的临时文件，请忽略
├── program                          #主程序依赖的可执行文件存放目录
│   ├── prepare_data_for_mysql       #c编译后的造数据程序，仅支持64位系统
│   └── SendMessage.sh               #发送消息的shell脚本
├── README.en.md
├── README.md                        #markdown格式的帮助文档
└── start.sh                         #主程序

4 directories, 14 files
root /data/git/prepare_data_for_mysql > 
```



#### 安装教程

1. 克隆项目
```
root /data/git > git clone https://gitee.com/mo-shan/prepare_data_for_mysql.git
```
2. 配置mysql连接信息
```
root /data/git > cd prepare_data_for_mysql/
root /data/git/prepare_data_for_mysql > vim conf/mysql.xml      #按照标签提示配置
```

3. 配置主配置文件
```
root /data/git/prepare_data_for_mysql > vim conf/prepare.xml    #按照提示配置
```
> 注：
- primary标签的value的值需要注意，可配置为true和false，表示是否truncate目标表，当row标签的max小于需要load的数据行时，这个值需要配置成false，且需要用户手动清空目标表(视具体情况而定，但是建议清空以确保测试数据干净)。
- row标签中的max表示最大行数，当需要load的数据行数大于该值则会将其分成多次load，比如有张表需要准备10w数据，max配置成5w，则会分成两次load，主要目的是避免大事务，或者网络包太大导致错误，建议配成1000w左右，是具体情况而定吧。
- algorithm标签组必须配置。

#### 使用说明

1. 使用
> 使用相对路径执行主程序
```
#准备测试表
mysql(test@localhost mysqld.sock)> CREATE TABLE `faceidentify` (
    ->   id int not null auto_increment primary key,
    ->   `uuid` varchar(32) NOT NULL DEFAULT 'uuid_string',
    ->   `IdType` varchar(3) NOT NULL,
    ->   `IdCode` varchar(18) NOT NULL DEFAULT 'car_string',
    ->   `Sex` smallint(6) NOT NULL DEFAULT '0',
    ->   `CreateTime` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
    ->   `AmendTime` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
    ->   `Reserve1` varchar(8) DEFAULT NULL,
    ->   `Reserve2` decimal(12,4) NOT NULL
    -> ) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;
Query OK, 0 rows affected (0.03 sec)

mysql(test@localhost mysqld.sock)> 


root /data/git/prepare_data_for_mysql > bash start.sh
```

2. 使用用例
```
root /data/git/prepare_data_for_mysql > bash start.sh                             #执行主程序
提示:若长时间无反应，可能是该服务器不能连接，请按下CTRL+C重新输入连接信息!
+++++++++++++++++++++++++++++++++++++++++++++++
本次连接验证成功，连接信息如下
+++++++++++++++++++++++++++++++++++++++++++++++
用户名：test
密  码：test
端  口：3311
主  机：172.28.85.43
+++++++++++++++++++++++++++++++++++++++++++++++
请注意:
该程序仅支持整型、字符型及时间类型的字段，
如存在其他类型将可能导致未知的错误！！！
请联系QQ：907771586
----------------------------------
0.结束运行。 
1.自动获取字段及各字段长度。
2.手动输入字段及各字段长度。
3.准备sysbench测试数据。
4.更改连接信息。
5.编辑配置文件。
----------------------------------
请输入您的选择:1                                                                   #建议选择1，即自动获取表结构
----------------------
服务器现有的数据库
----------------------
1.test
----------------------
输入r返回上一层菜单，请输入库名:1                                                    #选择目标表所在的库
----------------------
test库现有的表
----------------------
1.faceidentify
----------------------
输入r返回上一层菜单，请输入表名:1                                                     #选择目标表即可准备数据及load
您选择了faceidentify表
----------------------------------
          温馨提示
----------------------------------
当数据量较大，受网络或机器本身影响
较大，可能会导致load失败，
此时程序将会分小块load到各个分片。
默认阀值是1000w数据量，即当数据量
超过1000w时，程序自动将其分片！！！
当load失败时可以考虑加大MyQL网络包的配置:
set global max_allowed_packet=max_var;
----------------------------------

---------------------------------
第1个分片的连接信息是:
用户名：test
密  码：test
端口号：3311
主  机：172.28.85.43
第1个分片验证成功
---------------------------------

现在开始准备各分片的数据---
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
[2019-09-18 10:18:57] 开始准备第1个分片的数据, 进程号为:5849:
[2019-09-18 10:18:58] 请耐心等待，现已用时:1s
[2019-09-18 10:18:59] 第1个分片的数据准备完成
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

现在开始load各分片的数据---
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
[2019-09-18 10:18:59] load数据之前不清空该表!!!
[2019-09-18 10:18:59] 开始load第1个分片, 进程号为:5908
[2019-09-18 10:19:07] 请耐心等待，现已用时:7s
[2019-09-18 10:19:08] 第1个分片导入完成，下面开始验证数据量是否正确
[2019-09-18 10:19:08] 第1个分片, 验证结束
[2019-09-18 10:19:08] 第1个分片导入数据量与生成的数据量不符,请前往数据库检查
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
请输入任意键返回主菜单，或输入q退出程序:


```

```
mysql(test@localhost mysqld.sock)> select count(*) from faceidentify;
+----------+
| count(*) |
+----------+
|    10000 |
+----------+
1 row in set (0.00 sec)

mysql(test@localhost mysqld.sock)> select * from faceidentify limit 20;
+----+----------------------------------+--------+--------------------+------+----------------------------+----------------------------+----------+---------------+
| id | uuid                             | IdType | IdCode             | Sex  | CreateTime                 | AmendTime                  | Reserve1 | Reserve2      |
+----+----------------------------------+--------+--------------------+------+----------------------------+----------------------------+----------+---------------+
|  1 | 0d761e92-ddc6-4eab-b27c-4b9e2157 | 8      | 350625196808263201 | 5727 | 2007-01-16 11:11:27.122237 | 2008-04-24 14:54:22.165120 | 794923   | 48929246.6030 |
|  2 | 372979c6-7363-45bf-adaa-cf2b5f93 | 4      | 360734196706174315 | 2894 | 1993-09-13 15:17:53.111664 | 1994-09-28 22:45:31.190332 | 291853   | 46921516.7784 |
|  3 | 75fa6c15-f3f9-44cd-9cd3-c901a8ab | 8      | 610525199605172513 | 8924 | 2001-02-21 15:33:39.179700 | 2002-05-14 22:42:10.170919 | 892215   | 21272314.3989 |
|  4 | 651bd422-3d26-4994-98ec-192ced1d | 0      | 511921195008113578 | 3045 | 2011-09-12 21:16:31.195234 | 2012-07-27 13:50:18.114708 | 894213   | 69155073.8000 |
|  5 | 98dc664f-4402-4074-aa50-00a891a4 | 0      | 530827200605153558 | 4213 | 2002-07-15 11:35:31.186744 | 2003-06-26 22:58:38.170294 | 194140   | 51489628.0624 |
|  6 | 9a8c67ea-75bd-494e-8acf-1d20509d | 7      | 130726199102181721 | 4989 | 2014-09-20 17:26:47.104549 | 2015-05-10 17:58:20.115396 | 084547   | 77248526.1499 |
|  7 | 8799f3e1-86db-4213-b4c0-1bf5a351 | 0      | 141024199908282878 | 2794 | 2002-07-14 22:37:37.113188 | 2003-07-11 22:41:45.114714 | 876496   | 88617099.3471 |
|  8 | 2c0024b1-ac71-4755-88f4-33059e24 | 8      | 522632196204103060 | 4213 | 1998-07-21 14:37:13.192429 | 1999-08-17 10:57:41.107833 | 918530   | 66985140.3997 |
|  9 | 6b6b4757-6ead-4170-82f6-04a827d7 | 7      | 321112200909194161 | 9185 | 1995-02-27 20:55:22.188670 | 1996-05-28 15:33:45.198424 | 832894   |  5185326.3481 |
| 10 | 90f5ef4c-aa3d-4345-8a08-6dfec9d8 | 4      | 610330198006181611 | 8942 | 1994-08-23 15:45:15.112465 | 1995-03-11 19:47:31.199142 | 089221   | 27976141.3360 |
| 11 | d9ea84d6-8959-4a04-917d-5c88e164 | 2      | 450722197301114036 | 7733 | 2009-05-20 13:15:32.152564 | 2010-04-19 11:57:40.155893 | 866052   | 84730719.4099 |
| 12 | 80bec7fe-9aad-46b7-b95e-55390c7f | 4      | 350701199709273992 |  528 | 2007-06-14 23:50:50.120219 | 2008-09-10 18:24:22.162438 | 291853   | 21763255.8639 |
| 13 | 7d267ea4-6584-48e8-b025-5972c647 | 8      | 431322198004133019 | 4957 | 2009-07-22 13:46:39.179752 | 2010-01-13 13:26:17.172712 | 853008   | 84073659.9226 |
| 14 | 71823f0a-1cd2-4c9d-8b91-8a0a8b7d | 3      | 321201199106164865 | 5690 | 2007-01-26 13:29:43.114998 | 2008-07-17 14:39:42.188629 | 605283   | 60065062.6146 |
| 15 | 8cc328e7-6ffc-4a82-81a0-d3ac0d65 | 7      | 440982201105113713 | 5486 | 1996-08-17 14:27:41.145575 | 1997-07-15 15:30:57.149725 | 989247   | 63047662.5471 |
| 16 | f623578c-4d67-4c37-b1cd-d401ab8f | 2      | 130530195702224571 | 7337 | 1999-07-14 22:50:46.176434 | 2000-06-25 17:17:19.165031 | 832894   | 61703048.7714 |
| 17 | 04bc37ea-dd52-40cd-8d3f-4e9ef3ed | 4      | 542328198303212915 | 1406 | 2000-06-26 14:49:45.176450 | 2001-07-22 16:26:35.142434 | 185300   | 11076539.2608 |
| 18 | a1ecac53-d660-437d-b943-bb21f589 | 2      | 320124195402114789 | 2473 | 2008-08-21 10:13:25.107391 | 2009-02-26 21:50:39.155879 | 215876   | 92361457.4468 |
| 19 | 56e068f9-3cef-4091-9e09-fb602c95 | 2      | 431023201306284518 | 3008 | 2004-03-28 12:24:45.112483 | 2005-05-11 23:44:14.148973 | 892215   | 29565178.6640 |
| 20 | 99afc89e-78b2-4d65-86b2-42eda988 | 8      | 130921200701144164 | 9414 | 2007-03-11 13:33:39.136341 | 2008-07-19 23:43:12.105854 | 213761   | 39901031.5641 |
+----+----------------------------------+--------+--------------------+------+----------------------------+----------------------------+----------+---------------+
20 rows in set (0.00 sec)

mysql(test@localhost mysqld.sock)> 

```


> 附
- 如需要生成uuid的字符串，对建表语句有要求，要求该字段的默认值要带有"uuid_string"关键字，即column varchat(32) default '***uuid_string***'
- 如需要生成身份证的字符串，对建表语句有要求，要求该字段的默认值要带有"car_string"关键字,即col_name varchar(18) default '***car_string***'

