#!/bin/sh
#Author:moshan
#Function: Send message  
tty_file=$1
message_file=$2
while :
do  
    tty_=`cat ${tty_file}`
    tty_2=`sed 's/^.....//g' ${tty_file}`
    if_vim=`ps -ef|grep vim |grep "action.xml"|grep "${tty_2}"|grep -v grep|wc -l` 
    #if_vim保存的值是当前是否存在start.sh进程在编辑配置文件
    if_exist=`ps -ef|grep start|grep "${tty_2}"|grep -v grep|wc -l`
    #if_exist保存的值是start.sh进程是否还存在
    [ ${if_exist} -ne 1 ] && break
    if [ ${if_vim} -eq 1 ]
    then
        sleep 1
        echo -e "\033[31m`cat ${message_file}`注：该行是提示信息，不会保存至配置文件，不用理会直接修改配置文件即可。\033[0m" >>${tty_}
        break
    fi
done
